using UnityEngine;
using System.Collections;
using ExitGames.Client.Photon;
/// <summary>
/// Can be attached to a GameObject to show info about the owner of the PhotonView.
/// </summary>
/// <remarks>
/// This is a Photon.Monobehaviour, which adds the property photonView (that's all).
/// </remarks>
[RequireComponent(typeof(PhotonView))]
public class ShowInfoOfPlayer : Photon.MonoBehaviour
{
    private GameObject textGo;
    private TextMesh tm;
	public static string PlayerName;
    public float FontSize = 0;

    public Font font;
    public bool DisableOnOwnObjects;

    void Start()
    {
        if (font == null)
        {
            font = (Font)Resources.FindObjectsOfTypeAll(typeof(Font))[0];
            Debug.LogWarning("No font defined. Found font: " + font);
        }

        if (tm == null)
        {
            textGo = new GameObject("3d text");
            textGo.transform.parent = this.gameObject.transform;
			textGo.transform.localPosition = new Vector3(0, 2, 0);

            MeshRenderer mr = textGo.AddComponent<MeshRenderer>();
            mr.material = font.material;
            tm = textGo.AddComponent<TextMesh>();
            tm.font = font;
            tm.anchor = TextAnchor.MiddleCenter;
            if (this.FontSize > 0)
            {
                tm.characterSize = this.FontSize;
            }
        }
    }

    void Update()
    {
		bool showInfo = (!this.DisableOnOwnObjects && this.photonView.isMine) || !this.photonView.isMine;
        if (textGo != null)
        {
            textGo.SetActive(showInfo);
        }
        if (!showInfo)
        {
            return;
        }

		PhotonNetwork.player.name = PlayerName;
		PhotonPlayer owner = this.photonView.owner;

		if (owner != null)
        {
			tm.text = (string.IsNullOrEmpty(owner.name)) ? "player"+owner.ID : owner.name;
        }
        else if (this.photonView.isSceneView)
        {
            tm.text = "scn";
        }
        else
        {
            tm.text = "n/a";
        }
    }

	public void SetPlayerName(string playerName){
		PlayerName = playerName;
	}
}
