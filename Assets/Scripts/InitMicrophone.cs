﻿namespace com.Omniscope.AlterOffice {

	using UnityEngine;

	public class InitMicrophone : MonoBehaviour {
		#if UNITY_WEBGL && !UNITY_EDITOR
		void Awake() {
			Debug.Log("AWAKE IS CALLED!!!!");
			Microphone.Init();
			Microphone.QueryAudioInput();
		}
		#endif

		#if UNITY_WEBGL && !UNITY_EDITOR
		void Update() {
//			Microphone.Update();
		}
		#endif
	}
}