﻿namespace com.Omniscope.AlterOffice {

    using UnityEngine;
    using UnityEngine.UI;

    [RequireComponent(typeof(Toggle))]
    [DisallowMultipleComponent]
    public class BetterToggle : MonoBehaviour {
        private Toggle toggle;

        public delegate void OnToggle(Toggle toggle);

        public static event OnToggle ToggleValueChanged;

        private void Start() {
            toggle = GetComponent<Toggle>();
            toggle.onValueChanged.AddListener(delegate { OnToggleValueChanged(); });
        }

        public void OnToggleValueChanged() {
            if (ToggleValueChanged != null) {
                ToggleValueChanged(toggle);
            }
        }
    }
}