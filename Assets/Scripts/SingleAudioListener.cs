﻿namespace com.Omniscope.AlterOffice {

	using UnityEngine;

	public class SingleAudioListener : MonoBehaviour {
		private void Start() {
			PhotonView photonView = GetComponent<PhotonView>();
			if (photonView.isMine) {
				GetComponent<AudioListener> ().enabled = true;
			}
		}
	}
}
