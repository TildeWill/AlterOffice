﻿namespace com.Omniscope.AlterOffice {

    using UnityEngine;

    public class ThirdPersonController : BasePOVController {

        [SerializeField]
        private float movingTurnSpeed = 360;

        protected override void Move(float h, float v) {
            rigidBody.velocity = v * speed * transform.forward;
            transform.rotation *= Quaternion.AngleAxis(movingTurnSpeed * h * Time.deltaTime, Vector3.up);
        }
    }

}