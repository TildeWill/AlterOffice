﻿namespace com.Omniscope.AlterOffice {

    using UnityEngine;

    public class CharacterInstantiation : OnJoinedInstantiate {

        public delegate void OnCharacterInstantiated(GameObject character);

        public static event OnCharacterInstantiated CharacterInstantiated;

        public new void OnJoinedRoom() {
            if (this.PrefabsToInstantiate != null) {
				GameObject gameObject = PrefabsToInstantiate[(PhotonNetwork.player.ID - 1) % PrefabsToInstantiate.Length];
                //Debug.Log("Instantiating: " + o.name);
                Vector3 spawnPos = Vector3.zero;
                if (this.SpawnPosition != null) {
                    spawnPos = this.SpawnPosition.position;
                }
                Vector3 random = Random.insideUnitSphere;
                random = this.PositionOffset * random.normalized;
                spawnPos += random;
                spawnPos.y = 0;
                Camera.main.transform.position += spawnPos;

                gameObject = PhotonNetwork.Instantiate(gameObject.name, spawnPos, Quaternion.identity, 0);
                if (CharacterInstantiated != null) {
					CharacterInstantiated(gameObject);
                }
            }
        }
    }
}